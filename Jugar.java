package com.example.masteraaa.ppt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class Jugar extends AppCompatActivity {

    Spinner jugadaUsuario;
    ImageView jugadaIA;
    TextView puntuacion;
    Button jugar;
    Button volver;
    int tiradaIA;
    int nTiradas = 1;
    Integer[] imagenes = { R.drawable.piedra, R.drawable.papel,
            R.drawable.tijera };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugar);

        jugadaUsuario = (Spinner) findViewById(R.id.jugadaUsuario);
        jugadaIA = (ImageView) findViewById(R.id.jugadaIA);
        puntuacion = (TextView) findViewById(R.id.puntuacion);
        jugar = (Button) findViewById(R.id.jugar);
        volver = (Button) findViewById(R.id.volver);

        AdaptadorSpinner adaptador=new AdaptadorSpinner(this,imagenes);

        jugadaUsuario.setAdapter(adaptador);

        tiradaIA = imagenIA();

        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nTiradas<5) {
                    if(nTiradas==1){
                        jugar.setText("Jugar");
                        puntuacion.setText("0 - 0");
                    }else{
                        jugar.setText("Otra");
                        puntuacion.setText("0 - 0");
                    }
                    tiradaIA = imagenIA();
                    nTiradas++;
                }else if(nTiradas==5){
                    puntuacion.setText("0 - 0 \n Fin del Juego");
                    nTiradas = 1;
                    jugar.setText("Empezar otra  Vez");
                }
            }
        });

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private int imagenIA() {
        int i = (int)(Math.random()*3);
        switch (i){
            case 0: jugadaIA.setImageResource(R.drawable.piedra);
                break;
            case 1: jugadaIA.setImageResource(R.drawable.papel);
                break;
            case 2: jugadaIA.setImageResource(R.drawable.tijera);
        }
        return i;
    }
}
